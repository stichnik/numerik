package stichnik;

import java.util.Random;
import java.util.stream.IntStream;

class montecarlo {
	
	static void approximatePi(int n){
		Random rand = new Random();
		int inCircle=0;
		for(int i=0;i<n;i++){
			double x=rand.nextDouble();
			double y=rand.nextDouble();
			if((x*x)+(y*y)<=1)
				inCircle++;
		}
		double approximation = (double)inCircle/n*4;
		System.out.println("Approximated Pi with "+n+" iterations and got: "+approximation+"\nThis is off from actual pi by: "+(approximation-Math.PI));
	}
}
